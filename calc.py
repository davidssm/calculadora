import sys

def suma(dato1, dato2):
    if type(dato1) != int or type(dato2) != int: 
        sys.exit("Los dos argumentos deben ser números")
    return dato1 + dato2

def resta(dato1, dato2):
    if type(dato1) != int or type(dato2) != int: 
        sys.exit("Los dos argumentos deben ser números")
    return dato2 - dato1


if __name__ == '__main__':
    print(f"Primera suma 1 + 2: {suma(1, 2)}")
    print(f"Segunda suma 3 + 4: {suma(3, 4)}")
    print(f"Primera resta 6 - 5: {resta(5, 6)}")
    print(f"Segunda resta 8 - 7: {resta(7, 8)}")